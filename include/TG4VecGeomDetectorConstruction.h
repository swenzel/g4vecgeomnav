/// \file TG4VecGeomDetectorConstruction.h
/// \brief Definition of the TG4VecGeomDetectorConstruction and
///        TVirtualUserPostDetConstruction classes
///
/// \author S. Wenzel; CERN

#ifndef TG4VecGeomDetectorConstruction_HH
#define TG4VecGeomDetectorConstruction_HH

#include "TG4RootDetectorConstruction.h"
#include "G4VUserDetectorConstruction.hh"
#include "G4RotationMatrix.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include <map>
#include <VecGeom/management/GeoManager.h>
#include <TGeo2VecGeom/RootGeoManager.h>
#include <VecGeom/volumes/LogicalVolume.h>
#include <VecGeom/volumes/PlacedVolume.h>
#include <iostream>
#include "FastG4VecGeomLookup.h"

using GeoManager_t = vecgeom::GeoManager;
using LVolume_t    = vecgeom::LogicalVolume;
using PVolume_t    = vecgeom::VPlacedVolume;

class G4LogicalVolume;
class G4VPhysicalVolume;
class TVirtualUserPostDetConstruction;

/// \brief Builder creating a VecGeom-G4 geometry mapping
/// starting from an existing mapping of TGeo-pseudo G4 geometry.
///
class TG4VecGeomDetectorConstruction : public G4VUserDetectorConstruction {
private:
  /// conversions for logical volumes ----------

  /// the map from LVolume_t to G4LogicalVolume
  using LV_VGtoG4Map_t = std::map<const LVolume_t *, const G4LogicalVolume *>;
  LV_VGtoG4Map_t fLV_VGtoG4_Map; // mapping to G4 volumes

  /// the map from G4LogicalVolume to LVolume_t
  using LV_G4toVGMap_t = std::map<const G4LogicalVolume *, const LVolume_t *>;
  LV_G4toVGMap_t fLV_G4toVG_Map; // mapping to VecGeom volumes

  /// conversions for placed volumes ----------

  /// the map from PVolume_t to G4VPhysicalVolume
  using PV_VGtoG4Map_t = std::map<const PVolume_t *, const G4VPhysicalVolume *>;
  PV_VGtoG4Map_t fPV_VGtoG4_Map; //!< map from VecGeom to G4 physical volumes

  // a fast lookup using the fact that VecGeom placed volumes have
  // a contiguous linear index
  // std::vector<G4VPhysicalVolume*> fPV_VGtoG4_Vector;
  // the inverse is also true
  // std::vector<const PVolume_t*> fPV_G4toVG_Vector;

  FastG4VecGeomLookup fFastG4VGLookup;

  using PV_G4toVGMap_t = std::map<const G4VPhysicalVolume *, const PVolume_t *>;
  PV_G4toVGMap_t fPV_G4toVG_Map; //!< map from G4 to VecGeom placed volumes

protected:
  bool fIsConstructed                      = false;   ///< flag Construct() called
  GeoManager_t const *fGeometry            = nullptr; ///< TGeo geometry manager
  G4VPhysicalVolume *fTopPV                = nullptr; ///< World G4 physical volume
  TVirtualUserPostDetConstruction *fSDInit = nullptr; ///< Sensitive detector hook
  // accessing the ROOT detector construction
  TG4RootDetectorConstruction *fRootG4DetConstruction = nullptr;

public:
  // if someone already has a ROOT - G4 association we can easily construct the
  // VecGeom - G4 one
  TG4VecGeomDetectorConstruction(TG4RootDetectorConstruction &c)
      : G4VUserDetectorConstruction(), fRootG4DetConstruction(&c)
  {
  }

  ~TG4VecGeomDetectorConstruction() override;
  void ConnectVecGeomG4();

  FastG4VecGeomLookup const &GetFastG4VecGeomLookup() const { return fFastG4VGLookup; }

  // main detector construction interface
  G4VPhysicalVolume *Construct() override;

  // define sensitive det + field
  void ConstructSDandField() override;

  TG4RootDetectorConstruction *GetROOTDetConstruction() const { return fRootG4DetConstruction; }

  // Getters
  /// Return TGeo geometry manager
  G4VPhysicalVolume *GetTopPV() const { return fTopPV; }
  /*
  G4Material           *GetG4Material(const TGeoMaterial *mat)  const;
  G4LogicalVolume      *GetG4Volume(const LVolume_t *vol)      const;
  LVolume_t           *GetVolume(const G4LogicalVolume *g4vol) const;
  G4VPhysicalVolume    *GetG4VPhysicalVolume(const PVolume_t *node) const;
  PVolume_t             *GetNode(const G4VPhysicalVolume *g4vol) const;
                        /// Return the sensitive detector hook
  TVirtualUserPostDetConstruction        *GetSDInit() const {return fSDInit;}
                        /// Return the flag Construct() called
  Bool_t                IsConstructed() const {return fIsConstructed;}
*/
  void Initialize(TVirtualUserPostDetConstruction *sdinit = nullptr)
  {
    Construct();
    sdinit->Initialize(this);
  }

  static void CheckVolumeConsistency(PVolume_t const *v, G4VPhysicalVolume const *v2);

  friend class TG4VecGeomNavigator;
};

#endif
