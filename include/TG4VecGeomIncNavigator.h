/*
 * TG4VecGeomNavigator.h
 *
 *  Created on: Mar 1, 2018
 *      Author: swenzel
 */

#ifndef G4VECGEOM_INCLUDE_TG4VECGEOMINCNAVIGATOR_H_
#define G4VECGEOM_INCLUDE_TG4VECGEOMINCNAVIGATOR_H_

#include "G4Navigator.hh"
#include <VecGeom/navigation/NavigationState.h>
#include <VecGeom/base/Vector3D.h>
#include "G4ThreeVector.hh"

class TG4VecGeomDetectorConstruction;
struct FastG4VecGeomLookup;

/// \brief GEANT4 navigator using directly a VecGeom geometry but only in certain parts.
///
/// All navigation methods required by G4 tracking are implemented by
/// this class by invoking the corresponding functionality of VecGeom
///
/// \author S. Wenzel; CERN

//#define REPLACE_COMPUTESTEP 1
#define REPLACE_COMPUTESAFETY 1

class TG4VecGeomIncNavigator : public G4Navigator {

protected:
  const FastG4VecGeomLookup *fG4VGLookup; ///< G4VecGeom detector construction
                                          ///< which is the structure keeping geometry lookups etc.
  vecgeom::NavigationState *fCurState  = nullptr;
  vecgeom::NavigationState *fTestState = nullptr;

private:
  G4VPhysicalVolume *UpdateInternalHistory();
  void UpdateVecGeomState();
  void PrintState(); // print info about navigatio state (exiting, ...)

  const vecgeom::Vector3D<double> GetLocalVGPoint(G4ThreeVector const &) const;

public:
  TG4VecGeomIncNavigator() = default;
  TG4VecGeomIncNavigator(FastG4VecGeomLookup const &dc);
  ~TG4VecGeomIncNavigator() override = default;

#ifdef REPLACE_COMPUTESAFETY
  G4double ComputeSafety(const G4ThreeVector &globalpoint, const G4double pProposedMaxLength,
                         const G4bool keepState) override;
#endif

#ifdef REPLACE_COMPUTESTEP
  // Virtual methods for navigation
  G4double ComputeStep(const G4ThreeVector &pGlobalPoint, const G4ThreeVector &pDirection,
                       const G4double pCurrentProposedStepLength, G4double &pNewSafety) override;
#endif

  // helper function to check navigation histories
  void CheckStates() const;
};

inline const vecgeom::Vector3D<double> TG4VecGeomIncNavigator::GetLocalVGPoint(G4ThreeVector const &p) const
{
  const auto localpoint = fHistory.GetTopTransform().TransformPoint(p);
  return vecgeom::Vector3D<double>(localpoint[0], localpoint[1], localpoint[2]);
}

#endif /* G4VECGEOM_INCLUDE_TG4VECGEOMINCNAVIGATOR_H_ */
