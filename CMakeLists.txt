cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)
project(G4VecGeomNav)

if(NOT CMAKE_BUILD_TYPE)
    message(WARNING "CMAKE_BUILD_TYPE not set : will use RelWithDebInfo")
    set(CMAKE_BUILD_TYPE RelWithDebInfo)
endif()

find_package(Geant4 REQUIRED)
find_package(ROOT REQUIRED)
find_package(VecGeom REQUIRED)
find_package(TGeo2VecGeom REQUIRED)

#-------------------------------------------------q
# Locate sources for this project
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cxx)

# define the main target : The G4VecGeom library
add_library(g4vecgeomnav SHARED ${sources})
add_library(G4VecGeomNav::g4vecgeomnav ALIAS g4vecgeomnav)

target_include_directories(
        g4vecgeomnav
        PUBLIC
        ${Geant4_INCLUDE_DIR}
        ${ROOT_INCLUDE_DIRS}
        ${VECGEOM_INCLUDE_DIR}
        ${VECGEOM_EXTERNAL_INCLUDES}
        $<INSTALL_INTERFACE:include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)

# we depend on VecGeom / ROOT / G4
target_link_libraries(g4vecgeomnav
        PUBLIC
        ${Geant4_LIBRARIES} ${ROOT_LIBRARIES} ${VECGEOM_LIBRARIES}  TGeo2VecGeom::TGeo2VecGeom)


# build test applications
add_subdirectory(tests/FullCMS)


# installation specific
include(GNUInstallDirs)
set(INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/G4VecGeomNav)

install(TARGETS g4vecgeomnav
        EXPORT g4vecgeomnav-targets
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
        )

#This is required so that the exported target has the name G4VecGeomNav and not g4vecgeomnav
set_target_properties(g4vecgeomnav PROPERTIES EXPORT_NAME G4VecGeomNav)

install(DIRECTORY include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

#Export the targets to a script
install(EXPORT g4vecgeomnav-targets
        FILE
        G4VecGeomNavTargets.cmake
        NAMESPACE
        G4VecGeomNav::
        DESTINATION
        ${INSTALL_CONFIGDIR}
        )

#Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
#write_basic_package_version_file(
#        ${CMAKE_CURRENT_BINARY_DIR}/G4VecGeomNavConfigVersion.cmake
#        VERSION ${PROJECT_VERSION}
#        COMPATIBILITY AnyNewerVersion
#)

configure_package_config_file(${CMAKE_CURRENT_LIST_DIR}/cmake/G4VecGeomNavConfig.cmake.in
        ${CMAKE_CURRENT_BINARY_DIR}/G4VecGeomNavConfig.cmake
        INSTALL_DESTINATION ${INSTALL_CONFIGDIR}
        )

#Install the config, configversion and custom find modules
install(FILES
        ${CMAKE_CURRENT_BINARY_DIR}/G4VecGeomNavConfig.cmake
        # ${CMAKE_CURRENT_BINARY_DIR}/G4VecGeomNavConfigVersion.cmake
        DESTINATION ${INSTALL_CONFIGDIR}
        )

##############################################
## Exporting from the build tree

export(EXPORT g4vecgeomnav-targets FILE ${CMAKE_CURRENT_BINARY_DIR}/G4VecGeomNavTargets.cmake NAMESPACE G4VecGeomNav::)

#Register package in user's package registry
export(PACKAGE G4VecGeomNav)
